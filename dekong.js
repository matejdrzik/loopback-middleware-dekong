// Proximiio-DeKong Middleware
//
// Updates Request headers to handle both Kong and Loopback Authorization
//
// Kong Authorization: should be present as "Authorization: Bearer $JWT" form
// Loopback Authorization: should be present as "User-Authorization: $LOOPBACK_USER_TOKEN" form
//
// Author: Matej Drzik / Proximi.io <matej.drzik@quanto.sk>
// Created At: 2016-01-12

module.exports = function(options) {

  var AUTHORIZATION = 'authorization';
  var USER_AUTHORIZATION = 'user-authorization';
  var API_AUTHORIZATION = 'api-authorization';

  return function(req, res, next) {
    if (req.headers.hasOwnProperty(AUTHORIZATION)) {
      // request with authorization
      if (req.headers.hasOwnProperty(USER_AUTHORIZATION)) {
        // request having both kong & loopback authorization
        req.headers[API_AUTHORIZATION] = req.headers[AUTHORIZATION];
        req.headers[AUTHORIZATION] = req.headers[USER_AUTHORIZATION];
        next();
      } else {
        // request having only kong authorization, no loopback auth,
        // rename/remove kong authorization to prevent loopback confusion
        req.headers[API_AUTHORIZATION] = req.headers[AUTHORIZATION];
        delete req.headers[AUTHORIZATION];
        next();
      }
    } else {
      // request without authorization headers
      next();
    }
  }
};
